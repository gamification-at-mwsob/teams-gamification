package com.team.gamification.converter;

import com.team.gamification.dto.ClientDto;
import com.team.gamification.dto.TeamDto;
import com.team.gamification.model.Client;
import com.team.gamification.model.ClientSkill;
import com.team.gamification.model.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.stream.Collectors;

@Component
public class TeamConverter {
    private final SkillConverter skillConverter;

    @Autowired
    public TeamConverter(SkillConverter skillConverter) {
        this.skillConverter = skillConverter;
    }

    public TeamDto toTeamDto(Team team, Client client) {
        return TeamDto.builder()
                .id(team.getId())
                .name(team.getName())
                .clients(team.getClients().stream()
                        .map(teamClient -> this.convertTeamClientToDto(teamClient, !client.getId().equals(teamClient.getId())))
                        .collect(Collectors.toList()))
                .build();
    }

    private ClientDto convertTeamClientToDto(Client client, boolean filterSkills) {
        return ClientDto.builder()
                .teams(client.getTeams().stream()
                        .map(team -> TeamDto.builder()
                                .name(team.getName())
                                .id(team.getId())
                                .build())
                        .collect(Collectors.toList()))
                .skills(client.getClientSkills().stream()
                        .map(ClientSkill::getSkill)
                        .filter(skill -> (!filterSkills || skill.getRoles().isEmpty()) ||
                                !Collections.disjoint(skill.getRoles(), client.getRoles()))
                        .map(skill -> skillConverter.toSkillDto(skill, client))
                        .collect(Collectors.toList()))
                .id(client.getId())
                .wiw(client.getWiw())
                .build();
    }
}
