package com.team.gamification.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "client", schema = "gamification")
public class Client {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name="wiw", nullable = false)
    private String wiw;

    @Column(name="password", nullable = false)
    private String password;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
            name = "client_role",
            schema = "gamification",
            joinColumns = {@JoinColumn(name = "client_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")}
    )
    private List<Role> roles;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "client_team",
            schema = "gamification",
            joinColumns = {@JoinColumn(name = "client_id")},
            inverseJoinColumns = {@JoinColumn(name = "team_id")}
    )
    private List<Team> teams;

    @OneToMany(mappedBy = "client")
    private List<ClientSkill> clientSkills;
}
