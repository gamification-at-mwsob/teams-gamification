package com.team.gamification.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "client_skill", schema = "gamification")
public class ClientSkill {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    private Skill skill;

    @ManyToOne
    private Client client;

    @Column(name = "val", nullable = false)
    private Integer value;
}
