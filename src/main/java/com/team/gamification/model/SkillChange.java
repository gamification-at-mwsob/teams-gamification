package com.team.gamification.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "skill_change", schema = "gamification")
public class SkillChange {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "from_client_id", referencedColumnName = "id")
    private Client fromClient;

    @ManyToOne
    @JoinColumn(name = "to_client_id", referencedColumnName = "id")
    private Client toClient;

    @ManyToOne
    @JoinColumn(name = "skill_id", referencedColumnName = "id")
    private Skill skill;

    @Column(name = "val", nullable = false)
    private Integer value;
}
