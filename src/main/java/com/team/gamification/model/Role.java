package com.team.gamification.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "role", schema = "gamification")
public class Role {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "access_level", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private AccessLevel accessLevel;
}
