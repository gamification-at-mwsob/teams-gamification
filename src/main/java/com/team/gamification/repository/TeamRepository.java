package com.team.gamification.repository;

import com.team.gamification.model.Client;
import com.team.gamification.model.Team;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeamRepository extends CrudRepository<Team, Long> {
    List<Team> findTeamsByClientsContains(List<Client> clients);
}
