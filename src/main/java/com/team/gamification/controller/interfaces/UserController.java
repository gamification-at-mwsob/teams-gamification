package com.team.gamification.controller.interfaces;

import com.team.gamification.dto.ClientDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.security.Principal;

public interface UserController {
    @PostMapping("/user/{wiw}")
    ResponseEntity<ClientDto> getUser(Principal principal, @PathVariable(value = "wiw", required = false) String wiw);

    @GetMapping("/user/{wiw}")
    String getUserPage(@PathVariable(value = "wiw", required = false) String wiw);
}
