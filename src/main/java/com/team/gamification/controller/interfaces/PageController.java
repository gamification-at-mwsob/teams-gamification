package com.team.gamification.controller.interfaces;

import org.springframework.web.bind.annotation.GetMapping;

public interface PageController {
    @GetMapping("/")
    String page();
}
