import { Switch, Route } from 'react-router-dom'
const React = require('react');
const Login = require('./login');
const UserPage = require('./userPage');

export const Main = () => (
    <main>
        <Switch>
            <Route path='/login' component={Login}/>
            <Route path='/user/:wiw' component={UserPage}/>
        </Switch>
    </main>
);
