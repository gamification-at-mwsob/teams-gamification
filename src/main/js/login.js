const React = require('react');
const createReactClass = require('create-react-class');

var Login = createReactClass({
    render: function () {
        return (
            <div>
                <form method="post" action="/processAppLogin">
                    <p>
                        <label>Wiw:</label><br/>
                        <input type="text" name="wiw" required/>
                    </p>
                    <p>
                        <input type="password" value="pass" name="password" hidden="true" required/>
                    </p>
                    <input type="submit" value="Send"/>
                </form>
            </div>
        );
    }
});

module.exports = Login;
