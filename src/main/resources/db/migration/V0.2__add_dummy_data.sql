SET SEARCH_PATH TO GAMIFICATION;

/*
  Create dummy users with password 'pass'
*/
INSERT INTO gamification.client VALUES (1, 'dapankra', '$2a$04$fincQ.1S67ScYujCMWleBenh1VQAE0qzngLzED83fMRhsvrXToyuW') ON CONFLICT DO NOTHING;
INSERT INTO gamification.client VALUES (2, 'admin', '$2a$04$fincQ.1S67ScYujCMWleBenh1VQAE0qzngLzED83fMRhsvrXToyuW') ON CONFLICT DO NOTHING;

INSERT INTO gamification.skill  VALUES (1, 'Cool') ON CONFLICT DO NOTHING;
INSERT INTO gamification.skill  VALUES (2, 'Awesome') ON CONFLICT DO NOTHING;

INSERT INTO gamification.client_skill VALUES (1, 1, 1, 50) ON CONFLICT DO NOTHING;
INSERT INTO gamification.client_skill VALUES (2, 1, 2, 31) ON CONFLICT DO NOTHING;

INSERT INTO gamification.team VALUES (1, 'FirstTeam') ON CONFLICT DO NOTHING;

INSERT INTO gamification.client_team VALUES (1, 1, 1) ON CONFLICT DO NOTHING;
INSERT INTO gamification.client_team VALUES (1, 2, 1) ON CONFLICT DO NOTHING;

INSERT INTO gamification.role VALUES (1, 'ADMIN', 0) ON CONFLICT DO NOTHING;
INSERT INTO gamification.role VALUES (2, 'USER', 0) ON CONFLICT DO NOTHING;

INSERT INTO gamification.client_role VALUES (1, 1, 2) ON CONFLICT DO NOTHING;
