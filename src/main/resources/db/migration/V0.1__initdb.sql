CREATE SCHEMA IF NOT EXISTS gamification;

CREATE TABLE gamification.client (
  id BIGSERIAL NOT NULL,
  wiw VARCHAR(8) NOT NULL,
  password VARCHAR(64) NOT NULL,
  CONSTRAINT PK_client PRIMARY KEY (id)
);

CREATE TABLE gamification.role (
  id BIGSERIAL NOT NULL,
  name VARCHAR(50) NOT NULL,
  access_level INTEGER NOT NULL,
  CONSTRAINT PK_role PRIMARY KEY (id)
);

CREATE TABLE gamification.team (
  id BIGSERIAL NOT NULL,
  name VARCHAR(50) NOT NULL,
  CONSTRAINT PK_team PRIMARY KEY (id)
);

CREATE TABLE gamification.skill (
  id BIGSERIAL NOT NULL,
  name VARCHAR(50) NOT NULL,
  CONSTRAINT PK_skill PRIMARY KEY (id)
);

CREATE TABLE gamification.skill_change (
  id BIGSERIAL NOT NULL,
  from_client_id BIGSERIAL NOT NULL,
  to_client_id BIGSERIAL NOT NULL,
  skill_id BIGSERIAL NOT NULL,
  val INTEGER NOT NULL,
  CONSTRAINT PK_skill_change PRIMARY KEY (id),
  CONSTRAINT FK_skill_change_from_client FOREIGN KEY (from_client_id) REFERENCES gamification."client" (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_skill_change_to_client FOREIGN KEY (to_client_id) REFERENCES gamification."client" (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_skill_change_skill FOREIGN KEY (skill_id) REFERENCES gamification."skill" (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE gamification.client_team (
    id BIGSERIAL NOT NULL,
    client_id BIGSERIAL NOT NULL,
    team_id BIGSERIAL NOT NULL,
  CONSTRAINT PK_client_team PRIMARY KEY (id),
  CONSTRAINT FK_client_team_client FOREIGN KEY (client_id) REFERENCES gamification."client" (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_client_team_team FOREIGN KEY (team_id) REFERENCES gamification."team" (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE gamification.client_skill (
    id BIGSERIAL NOT NULL,
    client_id BIGSERIAL NOT NULL,
    skill_id BIGSERIAL NOT NULL,
    val INTEGER NOT NULL,
  CONSTRAINT PK_client_skill PRIMARY KEY (id),
  CONSTRAINT client_skill_val_positive CHECK (val >= 0),
  CONSTRAINT FK_client_skill_client FOREIGN KEY (client_id) REFERENCES gamification."client" (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_client_skill_skill FOREIGN KEY (skill_id) REFERENCES gamification."skill" (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE gamification.client_role (
    id BIGSERIAL NOT NULL,
    client_id BIGSERIAL NOT NULL,
    role_id BIGSERIAL NOT NULL,
  CONSTRAINT PK_client_role PRIMARY KEY (id),
  CONSTRAINT FK_client_role_client FOREIGN KEY (client_id) REFERENCES gamification."client" (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_client_role_role FOREIGN KEY (role_id) REFERENCES gamification."role" (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE gamification.skill_role (
    id BIGSERIAL NOT NULL,
    skill_id BIGSERIAL NOT NULL,
    role_id BIGSERIAL NOT NULL,
  CONSTRAINT PK_skill_role PRIMARY KEY (id),
  CONSTRAINT FK_skill_role_skill FOREIGN KEY (skill_id) REFERENCES gamification."skill" (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_skill_role_role FOREIGN KEY (role_id) REFERENCES gamification."role" (id) ON DELETE CASCADE ON UPDATE CASCADE
);
